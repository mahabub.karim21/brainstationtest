package com.example.androidapps2.repo.model

import retrofit2.Call
import retrofit2.http.GET

interface RepoApi {

    @GET("repositories?q=android")
    fun getRepoList(): Call<RepoResponse>
}